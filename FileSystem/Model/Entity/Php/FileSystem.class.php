<?php
/**
 * PHP FileSystem
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity\Php;

use ch\ugl\Library\FileSystem\Model\Entity\FileException;
use ch\ugl\Library\FileSystem\Model\Entity\FileSystemIterator;
use ch\ugl\Library\FileSystem\Model\Entity\Folder;
use ch\ugl\Library\FileSystem\Model\Entity\FolderException;
use ch\ugl\Library\FileSystem\Model\Entity\Path;
use ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException;

/**
 * Class FileSystem
 * @package ch\ugl\Library\FileSystem\Model\Entity\Php
 */
class FileSystem extends \ch\ugl\Library\FileSystem\Model\Entity\FileSystem
{
    /**
     * Construct the php file system
     *
     * @param string $path the base path for the php file system
     */
    public function __construct($path) {
        $this->path = new Path($path, $this);
    }

    /**
     * Check whether a specific item is a folder
     *
     * @param Path $path the path of the item to check whether it is a folder or not
     * @return bool
     */
    public function isDir($path)
    {
        return is_dir($path);
    }

    /**
     * Check whether a specific item is a file
     *
     * @param Path $path the path of the item to check whether it is a file or not
     * @return bool
     */
    public function isFile($path)
    {
        return is_file($path);
    }

    /**
     * List items under a specific path
     *
     * @param Path $path path object with current path
     * @param string $filter the regex which is used for filtering the items
     * @param bool $recursive if true, the items are listed recursively
     * @return FileSystemIterator
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FolderException
     *
     * @see \ch\ugl\Library\FileSystem\Model\Entity\AbstractFileSystem::ls();
     */
    public function ls($path, $filter = null, $recursive = false)
    {
        if (!$this->isDir($path)) {
            throw new FolderException("The folder does not exist");
        }
        $iterator = null;
        if ($recursive) {
            $directoryIterator = new \RecursiveDirectoryIterator($path);
            $iterator = new \RecursiveIteratorIterator($directoryIterator);
        } else {
            $iterator = new \DirectoryIterator($path);
        }
        if (!$filter) {
            return new FileSystemIterator($iterator, $this);
        }
        $regexIterator = new \RegexIterator($iterator, $filter, \RegexIterator::MATCH);
        return new FileSystemIterator($regexIterator, $this);
    }

    /**
     * Create a folder
     *
     * @param Path $path the path object for the directory which should be created
     * @param bool $recursive should we create parent folders if they don't exist
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FolderException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function mkdir($path, $recursive = false)
    {
        if ($this->isDir($path)) {
            throw new FolderException("The folder already exists");
        }
        if (!@mkdir($path, parent::getMode('folder'), $recursive)) {
            if (!$recursive) {
                // check whether target path exists
                if (!$this->isDir($path->getParent())) {
                    throw new FolderException("The target folder does not exist. Use recursive mode instead!");
                }
            }
            throw new PermissionDeniedException("PHP has no access to create the folder");
        }
    }

    /**
     * Deletes a folder
     *
     * @param Path $path the path to the folder which should be deleted
     * @param bool $recursive should directories and files be removed within the folders?
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FolderException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function rmdir($path, $recursive = false)
    {
        if (!$this->isDir($path)) {
            throw new FolderException("The folder does not exist");
        }
        if ($recursive) {
            foreach ($this->ls($path, null) as $fileSystemItem) {
                if ($fileSystemItem instanceof Folder) {
                    $this->rmdir($fileSystemItem->getPath(), true);
                } else {
                    $this->rm($fileSystemItem);
                }
            }
        }
        if (!@rmdir($path)) {
            throw new PermissionDeniedException("PHP has no access to remove the folder");
        }
    }

    /**
     * Touches folders and files and modifies the modification date / time
     *
     * @param Path $path the directory to touch
     * @param bool $recursive it should touch all items under the path?
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function touch($path, $recursive = false)
    {
        $fileHandler = null;
        // if the file does not exist, open it
        if (!$this->isFile($path) && !$this->isDir($path)) {
            $fileHandler = fopen($path, 'w');
            if (!$fileHandler) {
                throw new PermissionDeniedException("PHP has no access to create a file");
            }
        }

        // php does not support to set the modification time of folders with touch(), so the $recursive functionality
        // is not possible to use here

        // set new modification time
        if (!touch($path)) {
            throw new PermissionDeniedException("PHP has no access to touch a file");
        }

        // close handler
        if ($fileHandler) {
            fclose($fileHandler);
        }
    }

    /**
     * Deletes a file
     *
     * @param Path $path the path to the file which should be removed
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FileException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function rm($path)
    {
        if (!$this->isFile($path)) {
            throw new FileException("The file does not exist");
        }

        if (!unlink($path)) {
            throw new PermissionDeniedException("PHP has no access to remove the file");
        }
    }

    /**
     * Read a part of the content of a file or the whole file content
     *
     * @param Path $path the path of the file which should be read
     * @param int $offset the offset where the cursor should start to read
     * @param int $length the length of the string which should be returned
     * @return string the partly content of the file
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FileException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function read($path, $offset = 0, $length = 0)
    {
        if (!$this->isFile($path)) {
            throw new FileException("The file does not exist");
        }

        // open the file
        $fileHandler = fopen($path, 'r');
        if (!$fileHandler) {
            throw new PermissionDeniedException("PHP has no access to read from file");
        }

        // set cursor to offset position
        fseek($fileHandler, $offset);

        if ($length) {
            // get string from cursor (see up above) to cursor + $length
            $content = fgets($fileHandler, $length + 1);
        } else {
            $content = fgets($fileHandler);
        }

        // close file handler
        fclose($fileHandler);
        return $content;
    }

    /**
     * Write data to a file
     * @todo: writing to a specific position does not work yet
     *
     * @param Path $path the path to the file which should be accessed
     * @param string $data the content which should be added
     * @param int $offset the offset where to add the content
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FileException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function write($path, $data, $offset = 0)
    {
        if (!$this->isFile($path)) {
            throw new FileException("The file does not exist");
        }

        $this->touch($path);

        $fileHandler = fopen($path, 'a+');
        if (!$fileHandler) {
            throw new PermissionDeniedException("PHP has no access to write to file");
        }

        fseek($fileHandler, $offset);

        // this check must be === false because it also can return 0 when data was empty string
        if (fwrite($fileHandler, $data) === false) {
            throw new PermissionDeniedException("PHP has no access to write to file");
        }
        fclose($fileHandler);
    }

    /**
     * Changes the permission mode for a file system item
     * @todo: untested
     *
     * @param Path $path the path to the file system item
     * @param null|integer $newMod the new mode for the item
     * @param bool $recursive should the mode be set for all children of this item
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function chmod($path, $newMod = null, $recursive = false)
    {
        if (!$newMod) {
            $newMod = parent::getMode('file');
            if ($this->isDir($path)) {
                $newMod = parent::getMode('folder');
            }
        }

        if (strlen($newMod) === 3) {
            $newMod = 0 . $newMod;
        }

        if (!chmod($path, $newMod)) {
            throw new PermissionDeniedException("PHP has no access to change mode of item");
        }

        if (!$this->isDir($path)) {
            return;
        }

        // if it should make the operation recursively
        if ($recursive) {
            foreach ($this->ls($path, null) as $fileSystemItem) {
                $this->chmod($fileSystemItem->getPath(), $newMod, $recursive);
            }
        }
    }

    /**
     * Change the owner of a file
     * @todo: untested
     *
     * @param Path $path the path of the file system item
     * @param string $newOwner the user which should be the owner
     * @param bool $recursive should the new owner be set for all children of this item
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function chown($path, $newOwner, $recursive = false)
    {
        // change owner of object
        if (!chown($path, $newOwner)) {
            throw new PermissionDeniedException("PHP has no access to change owner of item");
        }

        // check whether it is a directory or not
        // if not, break here
        if (!$this->isDir($path)) {
            return;
        }

        // if it should make the operation recursively
        if ($recursive) {
            foreach ($this->ls($path, null) as $fileSystemItem) {
                $this->chmod($fileSystemItem->getPath(), $newOwner, $recursive);
            }
        }
    }

    /**
     * Change the group of a file
     * @todo: untested
     *
     * @param Path $path the path of the file system item
     * @param string $newGroup the user which should be the owner
     * @param bool $recursive should the new group be set for all children of this item
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function chgrp($path, $newGroup, $recursive = false)
    {
        // change owner of object
        if (!chown($path, $newGroup)) {
            throw new PermissionDeniedException("PHP has no access to change owner of item");
        }

        // check whether it is a directory or not
        // if not, break here
        if (!$this->isDir($path)) {
            return;
        }

        // if it should make the operation recursively
        if ($recursive) {
            foreach ($this->ls($path, null) as $fileSystemItem) {
                $this->chmod($fileSystemItem->getPath(), $newGroup, $recursive);
            }
        }
    }
}
