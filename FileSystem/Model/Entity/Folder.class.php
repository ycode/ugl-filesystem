<?php
/**
 * Folder
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class FolderException
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class FolderException extends \Exception
{
}

/**
 * Class Folder
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class Folder extends FileSystemItem
{
}
