<?php
/**
 * Path
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class PathException
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class PathException extends \Exception
{
}

/**
 * Class Path
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class Path
{
    const REGEX_DRIVE_LETTER = '[A-Z]\:';

    /**
     * @var string the base path provided from the file system
     */
    private $basePath;
    /**
     * @var string the clean relative path which is passed with the constructor down below
     */
    private $relativePath;
    /**
     * @var string the uncleaned original path which was passed by the constructor
     */
    private $path;
    /**
     * @var array possible directory separators which are replaced with the php default directory separator
     * @see Path::correctPath()
     */
    protected static $directorySeparators = array('\\', '/');
    /**
     * @var FileSystem
     */
    private $fileSystem;

    /**
     * Initialize the clean path object
     *
     * The constructor cleans up the provided path and divides the path into absolute base path
     * and relative path if provided
     *
     * After this division both parts will be cleaned up.
     *
     * @param string $path
     * @param FileSystem $fileSystem
     * @throws PathException
     */
    public function __construct($path, FileSystem $fileSystem)
    {
        // if the path is already a path object, then just take the relative path
        if ($path instanceof static) {
            $path = $path->getRelativePath();
        }
        $this->path = $path;
        $this->fileSystem = $fileSystem;

        // get base path, which was passed with the initialization of file system
        $fileSystemBasePath = str_replace(static::$directorySeparators, DIRECTORY_SEPARATOR, $this->fileSystem->getBasePath());
        $this->path = str_replace(static::$directorySeparators, DIRECTORY_SEPARATOR, $this->path);

        // is base path in path?
        if (
            (
                (
                    !empty($fileSystemBasePath) &&
                    strpos($this->path, $fileSystemBasePath) === false
                ) ||
                in_array($this->basePath, static::$directorySeparators)
            ) &&
            !self::startsWithDelimiter($this->path)
        ) {
            // no: is relative path
            $this->basePath = $fileSystemBasePath;
            $this->relativePath = $this->path;
        } elseif (self::startsWithDelimiter($this->path)) {
            // yes: is absolute path
            $this->basePath = $this->path;
        } else {
            // try to extract the base path from the original path
            $this->basePath = $fileSystemBasePath;
            $this->relativePath = substr($this->path, strlen($fileSystemBasePath));
        }
        
        // clean up the paths
        $this->correctPath($this->relativePath);
        $this->correctPath($this->basePath, true);

        // the relative path is still absolute, won't work because the defined root directory in file system handler
        // does not exist in $path which was an absolute one
        // the constructor could not resolve the path, throw an exception
        if (static::isAbsolutePath($this->relativePath)) {
            throw new PathException("Could not find clean relative path because the provided absolute path is not in space of file system root path!");
        }
    }

    /**
     * Append to relative path
     *
     * @param string $pathPart the path part to add after the existing relative path
     */
    public function append($pathPart) {
        $this->correctPath($pathPart);
        
        // if the relative path is empty we need no separator
        if (empty($this->relativePath)) {
            $this->relativePath .= $pathPart;
        } else {
            $this->relativePath .= DIRECTORY_SEPARATOR . $pathPart;
        }
    }

    /**
     * Get the last part of the path
     *
     * @return string the last part of the path, item name
     */
    public function getLastPart() {
        return end($this->getPathParts());
    }

    /**
     * Get path of parent item
     *
     * @return Path the path object of the parent item
     */
    public function getParent()
    {
        $parts = $this->getPathParts();
        array_pop($parts);
        return new self(implode(DIRECTORY_SEPARATOR, $parts), $this->fileSystem);
    }

    /**
     * Get the path parts
     *
     * @return array get parts of path as array
     */
    protected function getPathParts() {
        return explode(DIRECTORY_SEPARATOR, $this->getAbsolutePath());
    }

    /**
     * Cleans up the path
     *
     * If it is an absolute path, this method will add the disc drive letter or
     * the default php directory delimiter.
     * If it is a relative path, this method will remove the leading delimiter.
     *
     * In both cases, the script removes the ending delimiter.
     *
     * @param string $path the path which should be corrected
     * @param bool $isAbsolute should it be an absolute path?
     *
     * @link http://php.net/manual/en/dir.constants.php DIRECTORY_SEPARATOR definition
     */
    protected function correctPath(&$path, $isAbsolute = false)
    {
        if (empty($path)) {
            return;
        }
        if ($isAbsolute) {
            // is absolute path, should start with delimiter or drive letter
            // if not, try to add drive or delimiter
            $matches = array();
            if (!preg_match('#' . static::REGEX_DRIVE_LETTER . '#', $path) && !static::startsWithDelimiter($path)) {
                if (preg_match('#^(' . static::REGEX_DRIVE_LETTER . ')#', $_SERVER['DOCUMENT_ROOT'], $matches) && isset($matches[1])) {
                    $path = $matches[1] . DIRECTORY_SEPARATOR . $path;
                } else {
                    $path = DIRECTORY_SEPARATOR . $path;
                }
            }
        } else {
            // is relative path, should not start with delimiter
            if (static::startsWithDelimiter($path)) {
                $path = substr($path, 1);
            }
        }
        // remove ending delimiter anyway
        if (static::endsWithDelimiter($path)) {
            $path = substr($path, 0, -1);
        }
    }

    /**
     * Get the path as string (absolute or relative) path
     *
     * @param bool $absolute should the method return an absolute or relative path
     * @param \ch\ugl\Library\FileSystem\Model\Entity\FileSystem $fileSystem the file system object, which includes the delimiter and the base path
     * @throws PathException
     * @return string the absolute or relative path
     */
    public function toString($absolute = false, FileSystem $fileSystem = null)
    {
        // init path with relative path (if the caller wants to get the relative path)
        // otherwise it will be overwritten on the next few lines
        $path = $this->relativePath;
        
        // want to return an absolute path
        // if a file system is available, get the base path of file system
        // otherwise get the base path which is available in path object
        if ($absolute) {
            $path = $this->basePath;
            if ($fileSystem) {
                $path = $fileSystem->getBasePath();
            }
            
            if ($this->relativePath) {
                $path = $path . DIRECTORY_SEPARATOR . $this->relativePath;
            }
        }
        
        // no file system is available, just return the path with the PHP directory separator
        if (!$fileSystem) {
            return $path;
        }

        // replace the delimiter with the file system delimiter when a file system is available
        return str_replace(DIRECTORY_SEPARATOR, $fileSystem->getDelimiter(), $path);
    }

    /**
     * Get the relative path
     *
     * @return string
     */
    public function __toString()
    {
        // get relative path
        return $this->toString(true);
    }

    /**
     * Get the absolute path
     *
     * @param FileSystem $fileSystem
     * @return string
     */
    public function getAbsolutePath(FileSystem $fileSystem = null) {
        return $this->toString(true, $fileSystem);
    }

    /**
     * Get the relative path
     *
     * @return string the relative path
     */
    public function getRelativePath() {
        return $this->toString();
    }

    /**
     * Check whether a path is absolute or not
     *
     * It is absolute if the path has a drive letter or it begins with a (back-)slash.
     *
     * @param string $path the path to check
     * @return bool true if the path is absolute
     */
    protected static function isAbsolutePath($path)
    {
        return preg_match('#' . static::REGEX_DRIVE_LETTER . '#', $path) || static::startsWithDelimiter($path);
    }

    /**
     * Checks whether a path starts with the php default directory separator
     *
     * @param string $path path to check
     * @return bool true if the path starts with this delimiter and is probably absolute
     */
    protected static function startsWithDelimiter($path)
    {
        return substr($path, 0, 1) === DIRECTORY_SEPARATOR;
    }

    /**
     * Checks whether a path ends with the php default directory separator
     *
     * @param string $path path to check
     * @return bool true if the path ends with this delimiter
     */
    protected static function endsWithDelimiter($path)
    {
        return substr($path, -1, 1) === DIRECTORY_SEPARATOR;
    }
}
