<?php
/**
 * FileSystemItem
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class FileSystemItem
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
abstract class FileSystemItem
{
    /**
     * @var Path the path where the item is located in
     */
    protected $path = null;

    /**
     * Initilize File System Item
     *
     * @param Path $path the path of the item
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Get the relative path name
     *
     * @return string
     */
    public function getPathName()
    {
        return $this->path->toString();
    }

    /**
     * Get the path object
     *
     * @return Path the path object of the item
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Get the path of the parent folder as path object
     *
     * @return Path
     */
    public function getParent()
    {
        return $this->path->getParent();
    }

    /**
     * Get the file name (alias for FileSystemItem::getName())
     *
     * @return string
     * @see FileSystemItem::getName()
     */
    public function getFileName()
    {
        return $this->getName();
    }

    /**
     * Get the name (e.g. folder name or file name)
     *
     * @return string
     */
    public function getName()
    {
        return $this->path->getLastPart();
    }

    /**
     * Get the absolute path to the file
     *
     * @return string
     */
    public function getFullPath()
    {
        return $this->path->__toString();
    }
    
    /**
     * Get the name of the item
     * Is needed for RegexIterator for matching
     * @see http://us2.php.net/RegexIterator.accept
     */
    public function __toString()
    {
       return $this->getFullPath();
    }
}
