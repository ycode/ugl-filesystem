<?php
/**
 * Abstract FileSystem
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class PermissionDeniedException
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class PermissionDeniedException extends \Exception
{
}

/**
 * Class AbstractFileSystem
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class AbstractFileSystem extends FileSystem
{
    /**
     * @var array all loaded file systems, these systems are used as fallback for each previous file system
     */
    private $fileSystems = array();
    
    /**
     * Construct the abstract file system, which is used to persist
     * operations on multiple file systems which are used as fallback
     *
     * @param array $fileSystems the file systems which are available
     */
    public function __construct($fileSystems = array()) {
        $this->fileSystems = $fileSystems;
    }

    /**
     * Check whether a specific item is a folder
     *
     * @param Path $path the path of the item to check whether it is a folder or not
     * @return bool
     * @throws PermissionDeniedException
     */
    public function isDir($path)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                return $fileSystem->isDir($path);
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not check provided folder");
    }

    /**
     * Check whether a specific item is a file
     *
     * @param Path $path the path of the item to check whether it is a file or not
     * @return bool
     * @throws PermissionDeniedException
     */
    public function isFile($path)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                return $fileSystem->isFile($path);
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not check provided file");
    }

    /**
     * List items under a specific path
     *
     * @param string $path path with current path
     * @param string $filter the regex which is used for filtering the items
     * @param bool $recursive if true, the items are listed recursively
     * @return FileSystemIterator
     * @throws PermissionDeniedException
     */
    public function ls($path, $filter = null, $recursive = false)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                return $fileSystem->ls($path, $filter, $recursive);
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not access provided path");
    }

    /**
     * Create a folder
     *
     * @param string $path the path object for the directory which should be created
     * @param bool $recursive should we create parent folders if they don't exist
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function mkdir($path, $recursive = false)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->mkdir($path, $recursive);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not add folder");
    }

    /**
     * Deletes a folder
     *
     * @param string $path the path to the folder which should be deleted
     * @param bool $recursive should the file system remove the items recursively
     * @throws PermissionDeniedException
     */
    public function rmdir($path, $recursive = false)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->rmdir($path, $recursive);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not remove folder");
    }

    /**
     * Touches items
     *
     * @param string $path the path of the first item which will be touched
     * @param bool $recursive should the file system touch the items recursively
     * @throws PermissionDeniedException
     */
    public function touch($path, $recursive = false)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->touch($path, $recursive);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not touch item");
    }

    /**
     * Deletes a file
     *
     * @param string $path the path to the file
     * @throws PermissionDeniedException
     */
    public function rm($path)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->rm($path);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not remove file");
    }

    /**
     * Read a part of the content of a file or the whole file content
     *
     * @param string $path the path of the file which should be read
     * @param int $offset the offset where the cursor should start to read
     * @param int $length the length of the string which should be returned
     * @return string the partly content of the file
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function read($path, $offset = 0, $length = 0)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                return $fileSystem->read($path, $offset, $length);
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not read from file");
    }

    /**
     * Write data to a file
     *
     * @param string $path the path to the file which should be accessed
     * @param string $data the content which should be added
     * @param int $offset the offset where to add the content
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function write($path, $data, $offset = 0)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->write($path, $data, $offset);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not write to file");
    }

    /**
     * Changes the permission mode for a file system item
     *
     * @param string $path the path to the file system item
     * @param null|integer $newMod the new mode for the item
     * @param bool $recursive should the mode be set for all children of this item
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function chmod($path, $newMod = null, $recursive = false)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->chmod($path, $newMod, $recursive);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not change mode of item");
    }

    /**
     * Change the owner of a file
     *
     * @param string $path the path of the file system item
     * @param string $newOwner the user which should be the owner
     * @param bool $recursive should the new owner be set for all children of this item
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function chown($path, $newOwner, $recursive = false)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->chown($path, $newOwner, $recursive);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not change owner of item");
    }

    /**
     * Change the group of a file
     *
     * @param string $path the path of the file system item
     * @param string $newGroup the user which should be the owner
     * @param bool $recursive should the new group be set for all children of this item
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function chgrp($path, $newGroup, $recursive = true)
    {
        /**
         * @var FileSystem $fileSystem
         */
        foreach ($this->fileSystems as $fileSystem) {
            try {
                $path = new Path($path, $fileSystem);
                $fileSystem->chgrp($path, $newGroup, $recursive);
                return;
            } catch (PermissionDeniedException $e) {
            }
        }
        throw new PermissionDeniedException("Could not change group of item");
    }
}
