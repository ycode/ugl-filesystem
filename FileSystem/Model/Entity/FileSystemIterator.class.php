<?php
/**
 * FileSystemIterator
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class FileSystemIterator
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class FileSystemIterator implements \SeekableIterator
{
    /**
     * @var \ArrayIterator|\DirectoryIterator|\RecursiveDirectoryIterator
     */
    protected $baseIterator;
    /**
     * @var array
     */
    protected $notListedItems = array('.', '..');

    /**
     * @param \DirectoryIterator|\RecursiveDirectoryIterator|\ArrayIterator|\RegexIterator $iterator
     * @param FileSystem $fileSystem
     */
    public function __construct($iterator, $fileSystem)
    {
        $this->baseIterator = $iterator;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @param int $position
     */
    public function seek($position)
    {
        $this->rewind();
        for ($i = 0; $i < $position; $i++) {
            $this->next();
        }
    }

    public function rewind()
    {
        $this->baseIterator->rewind();
    }

    /**
     * @return File|Folder|mixed
     */
    public function current()
    {
        $item = $this->baseIterator->current();
        $path = $item->getFileName();
        if (in_array($path, $this->notListedItems)) {
            $this->next();
            return $this->current();
        }
        $fileSystemItem = null;
        $path = new Path($item->getPathName(), $this->fileSystem);
        if ($this->fileSystem->isDir($path)) {
            $fileSystemItem = new Folder($path);
        } else {
            $fileSystemItem = new File($path);
        }
        return $fileSystemItem;
    }

    /**
     * @return mixed|string
     */
    public function key()
    {
        return $this->baseIterator->key();
    }

    public function next()
    {
        $this->baseIterator->next();
    }

    public function valid()
    {
        return $this->baseIterator->valid();
    }
}
