<?php

namespace ch\ugl\Library\FileSystem\Model\Entity\Ftp;

use ch\ugl\Library\FileSystem\Model\Entity\File;
use ch\ugl\Library\FileSystem\Model\Entity\Folder;
use ch\ugl\Library\FileSystem\Model\Entity\Path;

class FileSystemIterator implements \SeekableIterator
{
    private $items = array();
    private $position = 0;
    private $baseDir;
    private $fileSystem;
    public function __construct($array, Path $path, FileSystem $fileSystem) {
        $this->items = $array;
        $this->baseDir = $fileSystem->getBasePath();
        $this->fileSystem = $fileSystem;
    }
    
    /**
     * @param int $position
     */
    public function seek($position)
    {
        $this->rewind();
        for ($i = 0; $i < $position; $i++) {
            $this->next();
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        $fileSystemItem = null;
        $path = new Path($this->items[$this->position], $this->fileSystem);
        if ($this->fileSystem->isDir($path->getRelativePath())) {
            $fileSystemItem = new Folder($path);
        } else {
            $fileSystemItem = new File($path);
        }
        return $fileSystemItem;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->position = 0;
    }
}
