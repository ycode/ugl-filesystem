<?php
/**
 * FTP FileSystem
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity\Ftp;
use ch\ugl\Library\FileSystem\Model\Entity\Ftp\FileSystemIterator as FtpFileSystemIterator;
use ch\ugl\Library\FileSystem\Model\Entity\FolderException;
use ch\ugl\Library\FileSystem\Model\Entity\Path;

/**
 * Class FtpConnectionException
 * @package ch\ugl\Library\FileSystem\Model\Entity\Ftp
 */
class FtpConnectionException extends \Exception
{
}

/**
 * Class FileSystem
 * @package ch\ugl\Library\FileSystem\Model\Entity\Ftp
 */
class FileSystem extends \ch\ugl\Library\FileSystem\Model\Entity\FileSystem
{
    /**
     * @var resource ftp connection resource
     */
    private $connection = null;
    /**
     * @var array the default credentials for the ftp connection
     */
    private $credentials = array(
        'host' => '127.0.0.1',
        'username' => '',
        'password' => '',
        'port' => '21',
        'passive' => false,
    );

    /**
     * Initialize the Ftp FileSystem
     *
     * @param string $path the base path for the php file system
     * @param array $credentials the credentials to use, these credentials overwrite the default credentials up above
     */
    public function __construct($path, $credentials = array()) {
        if ($credentials) {
            $this->setCredentials($credentials);
        }
        $this->path = new Path($path, $this);
    }

    /**
     * Close the open ftp connection if there is one
     */
    public function __destruct()
    {
        if ($this->connection) {
            ftp_close($this->connection);
        }
    }

    /**
     * Configure new credentials for the Ftp FileSystem
     *
     * @param array $credentials update the credentials
     */
    public function setCredentials($credentials)
    {
        $this->credentials = array_merge($this->credentials, $credentials);
    }

    /**
     * Opens a new connection to the ftp server and tries to log in
     *
     * @return resource the ftp connection resource
     * @throws FtpConnectionException
     */
    protected function getConnection()
    {
        // already connected
        if ($this->connection) {
            return $this->connection;
        }

        // connect to ftp
        $this->connection = @ftp_connect($this->credentials['host'], $this->credentials['port']);
        if (!$this->connection) {
            throw new FtpConnectionException("FTP: unable to connect to host");
        }

        // log in to ftp
        if (!@ftp_login($this->connection, $this->credentials['username'], $this->credentials['password'])) {
            throw new FtpConnectionException("FTP: unable to log in with provided credentials");
        }

        // activate / deactivate passive mode
        @ftp_pasv($this->connection, $this->credentials['passive']);

        return $this->connection;
    }

    public function isDir($path)
    {
        $this->getConnection();
        $currentDir = @ftp_pwd($this->connection);
        $ftpChangeDir = @ftp_chdir($this->connection, $path);
        @ftp_chdir($this->connection, $currentDir);
        return $ftpChangeDir;
    }

    public function isFile($path)
    {
        if (ftp_size($this->connection, $path) === -1) {
            return false;
        }
        return true;
    }

    /**
     * List items under a specific path
     *
     * @param Path $path path object with current path
     * @param string $filter the regex which is used for filtering the items
     * @param bool $recursive if true, the items are listed recursively
     * @return FileSystemIterator
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FolderException
     *
     * @see \ch\ugl\Library\FileSystem\Model\Entity\AbstractFileSystem::ls();
     */
    public function ls($path = null, $filter = null, $recursive = false)
    {
        $objects = $this->getFilesInDirectory($path, $recursive);
        $iterator = new FtpFileSystemIterator($objects, $path, $this);
        if (!$filter) {
            return new \ch\ugl\Library\FileSystem\Model\Entity\FileSystemIterator($iterator, $this);
        }
        $regexIterator = new \RegexIterator($iterator, $filter, \RegexIterator::MATCH);
        return new \ch\ugl\Library\FileSystem\Model\Entity\FileSystemIterator($regexIterator, $this);
    }

    /**
     * Get the files in a directory
     * 
     * This method is used for listing files, especially for recursive uses.
     * 
     * @param Path $path path object with current path
     * @param bool $recursive if true, the items are readed recursively
     * @throws FtpConnectionException
     * @return array
     */
    protected function getFilesInDirectory($path, $recursive = false) {
        $this->getConnection();
        if (!@ftp_chdir($this->connection, $path)) {
            throw new FtpConnectionException("FTP: could not open directory");
        }
        $objects = array();
        foreach (@ftp_nlist($this->connection, null) as $item) {
            $itemPath = clone $path;
            $itemPath->append($item);
            $objects[] = $itemPath;
            if ($recursive && $this->isDir($itemPath)) {
                $objects = array_merge($objects, $this->getFilesInDirectory($itemPath, $recursive));
            }
        }
        return $objects;
    }

    /**
     * Make a folder in a specific place
     * 
     * @param Path $path
     * @param bool $recursive should we create parent folders if they don't exist
     * @throws FolderException
     * @throws FtpConnectionException
     */
    public function mkdir($path, $recursive = false)
    {
        $this->getConnection();
        $newDirectoryPath = clone $this->path;
        $newDirectoryPath->append($path);
        //$newDirectoryPath = $newDirectoryPath->getRelativePath();
        if ($this->isDir($newDirectoryPath->getRelativePath())) {
            throw new FolderException("Folder does already exist");
        }
        $parentPath = $newDirectoryPath->getParent();
        while ($parentPath && !$this->isDir($parentPath->getRelativePath())) {
            $parentPath = $newDirectoryPath->getParent();
        }
        die();
        if (!@ftp_mkdir($this->connection, $newDirectoryPath->getRelativePath())) {
            throw new FtpConnectionException("FTP: could not create directory");
        }
    }

    /**
     * Deletes a folder
     *
     * @param Path $path the path to the folder which should be deleted
     * @param bool $recursive should directories and files be removed within the folders?
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FolderException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function rmdir($path, $recursive = false)
    {
        if (!$this->isDir($path)) {
            throw new FolderException("The folder does not exist");
        }
        if ($recursive) {
            foreach ($this->ls($path, null) as $fileSystemItem) {
                if ($fileSystemItem instanceof Folder) {
                    $this->rmdir($fileSystemItem->getPath(), true);
                } else {
                    $this->rm($fileSystemItem);
                }
            }
        }
        if (!@ftp_rmdir($this->connection, $path)) {
            throw new \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException("FTP has no access to remove the folder");
        }
    }
    
    /**
     * Touches folders and files and modifies the modification date / time
     *
     * @param Path $path the directory to touch
     * @param bool $recursive it should touch all items under the path?
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function touch($path, $recursive = false)
    {
        // create temporary file or php memory if available
        // load file into memory if it exists on server
        // upload file to server to modify modification time
    }

    /**
     * Deletes a file
     *
     * @param Path $path the path to the file which should be removed
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\FileException
     * @throws \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException
     */
    public function rm($path)
    {
        if (!$this->isFile($path)) {
            throw new \ch\ugl\Library\FileSystem\Model\Entity\FileException("The file does not exist");
        }

        if (!@ftp_delete($this->connection, $path)) {
            throw new \ch\ugl\Library\FileSystem\Model\Entity\PermissionDeniedException("FTP has no access to remove the file");
        }
    }

    public function read($path, $offset = 0, $length = 0)
    {
        // TODO: Implement read() method.
    }

    public function write($path, $data, $offset = 0)
    {
        // TODO: Implement write() method.
    }

    public function chmod($path, $newMod = 744, $recursive = false)
    {
        // TODO: Implement chmod() method.
    }

    public function chown($path, $newOwner, $recursive = false)
    {
        // TODO: Implement chown() method.
    }

    public function chgrp($path, $newGroup, $recursive = false)
    {
        // TODO: Implement chgrp() method.
    }
}
