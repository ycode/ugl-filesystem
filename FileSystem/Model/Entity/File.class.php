<?php
/**
 * File
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */
namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class FileException
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class FileException extends \Exception
{
}

/**
 * Class File
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class File extends FileSystemItem
{
}
