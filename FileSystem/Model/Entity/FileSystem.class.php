<?php

/**
 * FileSystem
 * @author Ueli Kramer <ueli.kramer@gmail.com>
 * @author Michael Ritter <drissg@gmail.com>
 * @version 1.0
 */

namespace ch\ugl\Library\FileSystem\Model\Entity;

/**
 * Class FileSystemException
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
class FileSystemException extends \Exception {
    
}

/**
 * Class FileSystem
 * @package ch\ugl\Library\FileSystem\Model\Entity
 */
abstract class FileSystem {

    /**
     * @var array the default modes for new files and folders
     */
    protected static $modes = array(
        'file' => 0664,
        'folder' => 0744,
    );

    /**
     * @var Path the base path
     */
    protected $path = '';

    /**
     * @var string the delimiter for the directories in this file system, e.g. Windows \ and Linux /
     */
    protected $delimiter = DIRECTORY_SEPARATOR;

    /**
     * Sets a new default permission mode for the object given
     * 
     * @param string $object the object name
     * @param float $newMode the new mode for the object type
     */
    public function setMode($object, $newMode) {
        if (!isset($this->modes[$object])) {
            throw new FileSystemException("there is no object named " . $object);
        }
        $this->modes[$object] = $newMode;
    }

    /**
     * Set multiple modes
     * 
     * @param array $modes the modes to set for objects given
     */
    public function setModes($modes) {
        foreach ($modes as $object => $newMode) {
            $this->setMode($object, $newMode);
        }
    }
    
    /**
     * Get the mode of an object, used for creation of object
     * 
     * @param string $object the object from which we want to get the default mode
     * @return float the mode
     * @throws FileSystemException
     */
    public function getMode($object) {
        if (!isset($this->modes[$object])) {
            throw new FileSystemException("there is no object named " . $object);
        }
        return $this->modes[$object];
    }

    /**
     * Get the base path
     *
     * @return string the path which is the base path for the current file system
     */
    public function getBasePath() {
        if ($this->path) {
            $absolutePath = $this->path->getAbsolutePath();
            return substr($absolutePath, 1);
        }
        return '';
    }

    /**
     * Get the delimiter of the current file system
     *
     * @return string delimiter of the file system
     */
    public function getDelimiter() {
        return $this->delimiter;
    }

    public abstract function isDir($path);

    public abstract function isFile($path);

    public abstract function mkdir($path);

    public abstract function rmdir($path, $recursive = false);

    public abstract function ls($path, $filter = null, $recursive = false);

    public abstract function touch($path, $recursive = false);

    public abstract function rm($path);

    public abstract function read($path, $offset = 0, $length = 0);

    public abstract function write($path, $data, $offset = 0);

    public abstract function chmod($path, $newMod = null, $recursive = false);

    public abstract function chown($path, $newOwner, $recursive = false);

    public abstract function chgrp($path, $newGroup, $recursive = false);
}
