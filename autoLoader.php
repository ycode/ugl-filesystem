<?php
define('BASE', dirname(__FILE__) . DIRECTORY_SEPARATOR);

function autoLoader($class)
{
    $prefix = 'ch\ugl\Library\\';
    $class = substr($class, strlen($prefix));
    $class = str_replace('\\', '/', $class);

    $class = str_replace('Exception', '', $class);

    if (file_exists(BASE . $class . '.class.php')) {
        require_once BASE . $class . '.class.php';
    }
}

spl_autoload_register('autoLoader');