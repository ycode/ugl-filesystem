<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ueli Kramer
 * Date: 10.10.13
 * Time: 12:25
 * To change this template use File | Settings | File Templates.
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'autoLoader.php';

class PathTest extends PHPUnit_Framework_TestCase {
    public function testAbsolutePathWithRelativeOne()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/test3/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test', $path->getAbsolutePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test', $path->getAbsolutePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test/', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test', $path->getAbsolutePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test/', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test', $path->getAbsolutePath());
    }

    public function testAbsolutePathWithAbsoluteOne()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/test3/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('basePath/test1/test2/test3/test4', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test4', $path->getAbsolutePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('basePath/test1/test2/test3/test4', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test4', $path->getAbsolutePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('basePath/test1/test2/test3/test4/', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test4', $path->getAbsolutePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('basePath/test1/test2/test3/test4/', $fileSystem);
        $this->assertEquals('\basePath\test1\test2\test3\test4', $path->getAbsolutePath());

    }

    public function testRelativePath()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/test3/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test4', $fileSystem);
        $this->assertEquals('test4', $path->getRelativePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test4/', $fileSystem);
        $this->assertEquals('test4', $path->getRelativePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test4/', $fileSystem);
        $this->assertEquals('test4', $path->getRelativePath());

        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test4', $fileSystem);
        $this->assertEquals('test4', $path->getRelativePath());
    }
    
    public function testToString()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/test3/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test1', $fileSystem);
        
        $this->assertEquals($path, '\basePath\test1\test2\test3\test1');
        
        
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('/test1', $fileSystem);
        
        $this->assertEquals($path, '\test1');
    }
    
    public function testAppend()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test3', $fileSystem);
        $path->append('test4');
        $this->assertEquals($path->toString(true), '\basePath\test1\test2\test3\test4');
        
        $path->append('test6');
        $this->assertEquals($path->toString(true), '\basePath\test1\test2\test3\test4\test6');
        $this->assertEquals($path->toString(), 'test3\test4\test6');
        
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('',  $fileSystem);
        $path->append('test11');
        $this->assertEquals($path->toString(true), '\basePath\test1\test2\test11');
        $this->assertEquals($path->toString(), 'test11');
    }
    
    public function testGetLastPart()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test3', $fileSystem);
        $this->assertEquals($path->getLastPart(), 'test3');
        
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path(null, $fileSystem);
        $this->assertEquals($path->getLastPart(), 'test2');
    }
    
    public function testGetParent()
    {
        $fileSystem = new ch\ugl\Library\FileSystem\Model\Entity\Php\FileSystem('/basePath/test1/test2/');
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path('test3', $fileSystem);
        $this->assertEquals($path->getParent(), '\basePath\test1\test2');
        
        $path = new \ch\ugl\Library\FileSystem\Model\Entity\Path(null, $fileSystem);
        $this->assertEquals($path->getParent(), '\basePath\test1');
    }
}
